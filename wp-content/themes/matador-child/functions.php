<?php
function theme_enqueue_styles() {
    $parent_style = 'matador';
    wp_enqueue_style( $parent_style , get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'matador-child', get_template_directory_uri() . '/style.css', array($parent_style));
}

add_action ('wp_enqueue_scripts', 'theme_enqueue_styles' );

/* Use the following code to remove the "You must be logged in to post a comment." at the bottom of the post to prevent the user to see login link */
add_filter( 'comment_form_defaults', function( $fields ) {
    $fields['must_log_in'] = sprintf( 
        __( '' ),
        wp_registration_url(),
        wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )   
    );
    return $fields;
});

/* Use the following script for the Google Analytics on Bikebuzz */
add_action('wp_footer', 'add_googleanalytics');

function add_googleanalytics() { ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1583609-16', 'auto');
  ga('send', 'pageview');

</script>
<?php }